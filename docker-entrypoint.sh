#!/bin/bash
## (We use sh since Alpine does not have Bash by default)


## exit immediately on commands with a non-zero exit status.
set -euo pipefail

## When started without any arguments, "-h", "--help", "-help" or "help", print
## usage.
if [ $# -eq 0 ] || [ x$1 == x"-h" ] || [ x$1 == x"--help" ] ||
   [ x$1 == x"-help" ] || [ x$1 == x"help" ]; then
    echo "      ModelCIF file formatting tool."
    echo "------------------------------------------"
    echo "Provided by SWISS-MODEL / Schwede group"
    echo "(swissmodel.expasy.org / schwedelab.org)"
    echo ""
    echo "This container takes a directory of "
    echo "ColabFold models and turns them into "
    echo "ModelCIF files."
    echo ""
    /usr/local/bin/translate2modelcif --help
    exit 1
fi
if [ x$1 == x"translate2modelcif" ] || [ x$1 == x"2cif" ]; then
    echo "Let's go to the CIF site of life!"
    shift
    # take over the process, make translate2modelcif run on PID 1
    exec /usr/local/bin/translate2modelcif $@
    exit $?
fi

exec "$@"

#  LocalWords:  euo pipefail eq Schwede schwedelab mmcif fi

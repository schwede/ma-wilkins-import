ARG VERSION_OST="2.3.0"
FROM registry.scicore.unibas.ch/schwede/openstructure:${VERSION_OST}
# We need to declare ARGs again which were declared before the build stage
# (FROM directive), otherwise they won't be available in this stage.
ARG VERSION_OST

## Set up environment
ENV SRC_DIR="/tmp" \
    VERSION_OST=${VERSION_OST} \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1


LABEL org.openstructure.base-image="${VERSION_OST}"
LABEL org.openstructure.translate2modelcif="2022-11-09.1"
LABEL maintainer="Stefan Bienert <stefan.bienert@unibas.ch>"
LABEL vendor1="Schwede Group (schwedelab.org)"
LABEL vendor2="SIB - Swiss Institute of Bioinformatics (sib.swiss)"
LABEL vendor3="Biozentrum - University of Basel (biozentrum.unibas.ch)"


## Install python-modelcif and python-ihm
COPY requirements.txt ${SRC_DIR}
WORKDIR ${SRC_DIR}
RUN set -e pipefail; \
    apt-get update -y; \
    apt-get install -y git pip; \
    pip install -r requirements.txt; \
    git clone https://github.com/ihmwg/python-ihm.git ihm.git; \
    cd ihm.git; \
    python3 setup.py build; \
    python3 setup.py install; \
    rm -rf ${SRC_DIR}/ihm.git; \
    cd ${SRC_DIR}; \
    git clone https://github.com/ihmwg/python-modelcif.git modelcif.git; \
    cd modelcif.git; \
    python3 setup.py build; \
    python3 setup.py install; \
    rm -rf ${SRC_DIR}/modelcif.git; \
    apt-get remove -y git pip; \
    apt-get -y autoremove

# switch to development version via build-arg
ARG ADD_DEV
RUN set -e pipefail; \
    if test xYES = x`echo ${ADD_DEV} | tr '[:lower:]' '[:upper:]'`; then \
      apt-get install -y pip emacs less; \
      pip install black pylint; \
      apt-get remove -y pip; \
      apt-get -y autoremove; \
    fi

## Copy tool(s)
COPY --chmod=755 translate2modelcif.py /usr/local/bin/translate2modelcif

## Add a dedicated user
## MMCIF_USER_ID can be used to avoid file permission issues.
ARG MMCIF_USER_ID=501
RUN adduser --system -u ${MMCIF_USER_ID} mmcif


COPY --chmod=755 docker-entrypoint.sh /

USER mmcif

ENTRYPOINT ["/docker-entrypoint.sh"]

# LocalWords:  ARG OST ARGs ENV SRC tmp PYTHONUNBUFFERED Schwede schwedelab py
# LocalWords:  PYTHONDONTWRITEBYTECODE Bioinformatics sib swiss Biozentrum ihm
# LocalWords:  modelcif txt WORKDIR pipefail chmod adduser mmcif ENTRYPOINT cd
# LocalWords:  pylint rf entrypoint
